import { defineStore } from 'pinia';


export const useCurrentWordStore = defineStore('currentWord', {
    state: () => ({ currentWord: '' }),
    actions: {
        updateCurrentWord(key: string) {
            this.currentWord += key.toLowerCase();
        },
        backspace() {
            this.currentWord = this.currentWord.slice(0, -1);
        },
    },
});