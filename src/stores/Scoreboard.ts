import { defineStore } from 'pinia';


export const useScoreBoardStore = defineStore('scoreBoard', {
    state: (): { score: number, foundWords: string[] } => ({
        score: 0,
        foundWords: [],
    }),
    actions: {
        addFoundWord(word: string) {
            if (this.foundWords.includes(word)) return false;

            this.foundWords.push(word);
            this.incrementScore(word);

            return true;
        },
        incrementScore(word: string) {
            this.score += word.length;
        },
    },
});