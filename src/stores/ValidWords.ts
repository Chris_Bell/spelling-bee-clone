import { defineStore, storeToRefs } from 'pinia';
import { useCurrentWordStore } from './CurrentWord';
import { useScoreBoardStore } from './Scoreboard';


export const useValidWordsStore = defineStore('validWords', {
    state: () => ({
        letters: ['g', 'd', 'h', 'n', 'o', 't', 'u'],
        requiredLetter: 'g',
        validWords: [
            'doughnut',
            'goon',
            'doggo',
            'dong',
            'dough',
            'dogong',
            'dugout',
            'dung',
            'godhood',
            'gong',
            'good',
            'goth',
            'gout',
            'hotdog',
            'hung',
            'nought',
            'ought',
            'outgo',
            'outgun',
            'outthought',
            'thong',
            'though',
            'thought',
            'thug',
            'tong',
            'tough',
            'unhung',
            'unthought',
        ],
    }),
    actions: {
        checkCurrentWord() {
            const { currentWord } = storeToRefs(useCurrentWordStore());

            if (this.validWords.includes(currentWord.value)) {
                const { addFoundWord } = useScoreBoardStore();
                addFoundWord(currentWord.value);
            }

            currentWord.value = '';
        },
    },
});