export function isLetterOfAlphabet(input: string): boolean {
    if (input.length != 1) {
        return false;
    }

    const matches = input.toLowerCase().match(/[a-z]/i);

    return matches != null && matches.length > 0;
}